package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.business.services.api.EnquiriesService;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import com.econetwireless.utils.constants.SystemConstants;
import com.econetwireless.utils.enums.ResponseCode;
import com.econetwireless.utils.messages.AirtimeBalanceResponse;
import com.econetwireless.utils.pojo.INBalanceResponse;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Administrator
 */
@RunWith(MockitoJUnitRunner.class)
public class EnquiriesServiceImplTest {

    @Mock
    private ChargingPlatform chargingPlatform;
    @Mock
    private SubscriberRequestDao subscriberRequestDao;

    private String partnerCode;
    private EnquiriesService enquiriesService;

    //Request
    private SubscriberRequest subscriberRequest;
    private Long id;
    private String requestType;
    private double balanceBefore;
    private double balanceAfter;
    private Date dateCreated;
    private Date dateLastUpdated;
    private String status;
    private String reference;
    private long version;

    private INBalanceResponse iNBalanceResponse;

//Response
    private AirtimeBalanceResponse expResult;
    private String responseCode;
    private String narrative;
    private String msisdn;
    private double amount;

    @Before
    public void setUp() {
        enquiriesService = new EnquiriesServiceImpl(chargingPlatform, subscriberRequestDao);
        loadRequest();
        loadResult();

    }

    private void loadRequest() {
        responseCode = ResponseCode.SUCCESS.getCode();
        narrative = "Your account balance is ";
        amount = 0.45d;

        msisdn = "263777222683";
        partnerCode = "CODE001";

        balanceAfter = 20d;
        dateCreated = new Date();
        dateLastUpdated = new Date();
        balanceBefore = 10d;
        msisdn = "263777222683";
        id = 1L;

        reference = "TEST123";
        requestType = SystemConstants.REQUEST_TYPE_AIRTIME_BALANCE_ENQUIRY;
        status = SystemConstants.STATUS_SUCCESSFUL;
        version = 2L;

        subscriberRequest = new SubscriberRequest();
        subscriberRequest.setAmount(amount);
        subscriberRequest.setBalanceAfter(balanceAfter);
        subscriberRequest.setBalanceBefore(balanceBefore);
        subscriberRequest.setDateCreated(dateCreated);
        subscriberRequest.setDateLastUpdated(dateLastUpdated);
        subscriberRequest.setId(id);
        subscriberRequest.setMsisdn(msisdn);
        subscriberRequest.setPartnerCode(partnerCode);
        subscriberRequest.setReference(reference);
        subscriberRequest.setRequestType(requestType);
        subscriberRequest.setStatus(status);
        subscriberRequest.setVersion(version);

        iNBalanceResponse = new INBalanceResponse();
        iNBalanceResponse.setAmount(amount);
        iNBalanceResponse.setMsisdn(msisdn);
        iNBalanceResponse.setNarrative(narrative);
        iNBalanceResponse.setResponseCode(responseCode);

    }

    private void loadResult() {

        expResult = new AirtimeBalanceResponse();
        expResult.setResponseCode(iNBalanceResponse.getResponseCode());
        expResult.setNarrative(iNBalanceResponse.getNarrative());
        expResult.setMsisdn(msisdn);
        expResult.setAmount(iNBalanceResponse.getAmount());

    }

    /**
     * Test of enquire method, of class EnquiriesServiceImpl.
     */
    @Test
    public void testEnquire() {
        System.out.println("enquire");
        when(subscriberRequestDao.save(any(SubscriberRequest.class))).thenReturn(subscriberRequest);
        when(chargingPlatform.enquireBalance(partnerCode, msisdn)).thenReturn(iNBalanceResponse);
        AirtimeBalanceResponse result = enquiriesService.enquire(partnerCode, msisdn);
        assertNotNull(result);
        assertEquals(expResult.getMsisdn(), result.getMsisdn());
        verify(subscriberRequestDao, times(1)).save(subscriberRequest);
    }

}
