package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.services.api.ReportingService;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Administrator
 */
@RunWith(MockitoJUnitRunner.class)
public class ReportingServiceImplTest {

    @Mock
    private SubscriberRequestDao subscriberRequestDao;

    private ReportingService reportingService;

    //
    private SubscriberRequest subscriberRequest;
    private double amount;
    private double balanceAfter;
    private Date dateCreated;
    private Date dateLastUpdated;
    private double balanceBefore;
    private String msisdn;
    private Long id;
    private String partnerCode;
    private String reference;
    private String requestType;
    private String status;
    private long version;

    private List<SubscriberRequest> expResult;

    @Before
    public void setUp() {
        reportingService = new ReportingServiceImpl(subscriberRequestDao);
        loadRequest();
        loadResult();
    }

    private void loadRequest() {
        partnerCode = "099099";
    }

    private void loadResult() {
        amount = 30d;
        balanceAfter = 20d;
        dateCreated = new Date();
        dateLastUpdated = new Date();
        balanceBefore = 10d;
        msisdn = "263777222683";
        id = 1L;

        reference = "TEST123";
        requestType = "AIRTIME";
        status = "Successful";
        version = 2L;

        subscriberRequest = new SubscriberRequest();
        subscriberRequest.setAmount(amount);
        subscriberRequest.setBalanceAfter(balanceAfter);
        subscriberRequest.setBalanceBefore(balanceBefore);
        subscriberRequest.setDateCreated(dateCreated);
        subscriberRequest.setDateLastUpdated(dateLastUpdated);
        subscriberRequest.setId(id);
        subscriberRequest.setMsisdn(msisdn);
        subscriberRequest.setPartnerCode(partnerCode);
        subscriberRequest.setReference(reference);
        subscriberRequest.setRequestType(requestType);
        subscriberRequest.setStatus(status);
        subscriberRequest.setVersion(version);

        expResult = new ArrayList<>();
        expResult.add(subscriberRequest);
    }

    /**
     * Test of findSubscriberRequestsByPartnerCode method, of class
     * ReportingServiceImpl.
     */
    @Test
    public void testFindSubscriberRequestsByPartnerCode() {
        System.out.println("findSubscriberRequestsByPartnerCode");
        when(subscriberRequestDao.findByPartnerCode(partnerCode)).thenReturn(expResult);
        List<SubscriberRequest> result = reportingService.findSubscriberRequestsByPartnerCode(partnerCode);
        assertEquals(expResult, result);
    }

}
