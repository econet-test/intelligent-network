package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.business.services.api.CreditsService;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import com.econetwireless.utils.constants.SystemConstants;
import com.econetwireless.utils.enums.ResponseCode;
import com.econetwireless.utils.messages.AirtimeTopupRequest;
import com.econetwireless.utils.messages.AirtimeTopupResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author brighton
 */
@RunWith(MockitoJUnitRunner.class)
public class CreditsServiceImplTest {

    @Mock
    private ChargingPlatform chargingPlatform;
    @Mock
    private SubscriberRequestDao subscriberRequestDao;

    private CreditsService creditsService;
    //Request
    private AirtimeTopupRequest airtimeTopupRequest;
    private double amount;
    private String partnerCode;
    private String referenceNumber;
    //
    private SubscriberRequest subscriberRequest;
    private Long id;
    private String requestType;
    private double balanceBefore;
    private double balanceAfter;
    private Date dateCreated;
    private Date dateLastUpdated;
    private String status;
    private String reference;
    private long version;

    //Response
    private AirtimeTopupResponse expResult;
    private String responseCode;
    private String narrative;
    private String msisdn;
    private double balance;

    private INCreditResponse inCreditResponse;

    @Before
    public void setUp() {
        creditsService = new CreditsServiceImpl(chargingPlatform, subscriberRequestDao);
        loadRequest();
        loadResult();
    }

    private void loadRequest() {
        msisdn = "263777222683";
        responseCode = ResponseCode.SUCCESS.getCode();
        narrative = "Test narrative";
        balance = 3.45d;
        partnerCode = "CODE001";
        amount = 3d;
        referenceNumber = "123456";

        airtimeTopupRequest = new AirtimeTopupRequest();
        airtimeTopupRequest.setAmount(amount);
        airtimeTopupRequest.setMsisdn(msisdn);
        airtimeTopupRequest.setPartnerCode(partnerCode);
        airtimeTopupRequest.setReferenceNumber(referenceNumber);

        balanceAfter = 20d;
        dateCreated = new Date();
        dateLastUpdated = new Date();
        balanceBefore = 10d;
        msisdn = "263777222683";
        id = 1L;

        requestType = SystemConstants.REQUEST_TYPE_AIRTIME_TOPUP;
        status = SystemConstants.STATUS_SUCCESSFUL;
        version = 2L;
        reference = referenceNumber;

        subscriberRequest = new SubscriberRequest();
        subscriberRequest.setAmount(amount);
        subscriberRequest.setBalanceAfter(balanceAfter);
        subscriberRequest.setBalanceBefore(balanceBefore);
        subscriberRequest.setDateCreated(dateCreated);
        subscriberRequest.setDateLastUpdated(dateLastUpdated);
        subscriberRequest.setId(id);
        subscriberRequest.setMsisdn(msisdn);
        subscriberRequest.setPartnerCode(partnerCode);
        subscriberRequest.setReference(reference);
        subscriberRequest.setRequestType(requestType);
        subscriberRequest.setStatus(status);
        subscriberRequest.setVersion(version);

    }

    private void loadResult() {
        inCreditResponse = new INCreditResponse();
        inCreditResponse.setBalance(balance);
        inCreditResponse.setMsisdn(msisdn);
        inCreditResponse.setResponseCode(responseCode);
        inCreditResponse.setNarrative(narrative);

        expResult = new AirtimeTopupResponse();
        expResult.setBalance(balance);
        expResult.setMsisdn(msisdn);
        expResult.setNarrative(narrative);
        expResult.setResponseCode(responseCode);

    }

    /**
     * Test of credit method, of class CreditsServiceImpl.
     */
    @Test
    public void testCredit() {
        System.out.println("credit");
        when(subscriberRequestDao.save(any(SubscriberRequest.class))).thenReturn(subscriberRequest);
        when(chargingPlatform.creditSubscriberAccount(any(INCreditRequest.class))).thenReturn(inCreditResponse);
        AirtimeTopupResponse result = creditsService.credit(airtimeTopupRequest);
        assertNotNull(result);
        assertEquals(expResult.getResponseCode(), result.getResponseCode());
    }

}
