package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.services.api.PartnerCodeValidator;
import com.econetwireless.epay.dao.requestpartner.api.RequestPartnerDao;
import com.econetwireless.epay.domain.RequestPartner;
import com.econetwireless.utils.execeptions.EpayException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Administrator
 */
@RunWith(MockitoJUnitRunner.class)
public class PartnerCodeValidatorImplTest {

    @Mock
    private RequestPartnerDao requestPartnerDao;

    private PartnerCodeValidator partnerCodeValidator;
    private String partnerCode;
    private boolean expResult;

    private RequestPartner rp;

    //RequestPartner Values
    private Long id;
    private String code;
    private String name;
    private String description;

    @Before
    public void setUp() {
        partnerCodeValidator = new PartnerCodeValidatorImpl(requestPartnerDao);
        loadRequest();
        loadResult();
    }

    @Test
    public void testValidatePartnerCodeShouldFailIfCodeIsEmpty() {
        System.out.println("testValidatePartnerCodeShouldPassIfCodeisProvided ... ...");
        partnerCode = null;
        boolean result = false;
        try {
            result = partnerCodeValidator.validatePartnerCode(partnerCode);
        } catch (EpayException n) {

        }
        assertNotEquals(expResult, result);
    }

    @Test
    public void testValidatePartnerCodeShouldPassIfCodeisProvided() {
        System.out.println("Validate Partner Code ... ...");
        when(requestPartnerDao.findByCode(partnerCode)).thenReturn(rp);
        boolean result = partnerCodeValidator.validatePartnerCode(partnerCode);
        assertEquals(expResult, result);
    }

    private void loadRequest() {
        partnerCode = "090909";
    }

    private void loadResult() {
        expResult = true;
        id = 1L;
        code = "CC00";
        name = "Test";
        description = "Test Test";
        rp = new RequestPartner();
        rp.setId(id);
        rp.setCode(code);
        rp.setDescription(description);
        rp.setName(name);
    }

}
